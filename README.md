# BPM Tap Tool

A tool for finding the beats per minute of a song (of anything) by tapping.

![Screenshot 1](./.github/s1.png) ![Screenshot 2](./.github/s2.png)

#### Installation

```shell
npm install
electron-packager . bpm-tap --platform=linux,win32 --arch=x64,ia32 --asar=true --out=bin \
    --ignore='dist\/' \
    --ignore='bin\/' \
    --ignore=".github\/" \
    --ignore='README.md' \
    --ignore='bundle.sh' \
    --ignore='.*~'
```

If you don't want to setup an Electron development environment, grep some binaries
from the **Releases** tab.

#### Hint

Project "stolen" from [lgarron/bpm-tap](https://github.com/lgarron/bpm-tap)
and converted into an Electron app which works offline. I improved the code and user interface a bit to make it look more clear ─ removed some clutter too.
