#!/usr/bin/env sh

electron-packager . bpm-tap --platform=linux,win32 --arch=x64,ia32 --asar=true --out=bin \
    --ignore='dist\/' \
    --ignore='bin\/' \
    --ignore=".github\/" \
    --ignore='README.md' \
    --ignore='bundle.sh' \
    --ignore='.*~'

cd bin

export GZIP=-9

tar cvzf bpm-tap-linux-ia32.tgz bpm-tap-linux-ia32
tar cvzf bpm-tap-linux-x64.tgz bpm-tap-linux-x64

zip -9 -r bpm-tap-win32-ia32.zip bpm-tap-win32-ia32
zip -9 -r bpm-tap-win32-x64.zip bpm-tap-win32-x64

rm -r bpm-tap-linux-ia32
rm -r bpm-tap-linux-x64
rm -r bpm-tap-win32-ia32
rm -r bpm-tap-win32-x64
