const electron = require('electron');
const pjson = require(__dirname + '/package.json');

const app = electron.app;
const Menu = electron.Menu;
const Tray = electron.Tray;
const BrowserWindow = electron.BrowserWindow;

let mainWindow = null;
let tray = null;

function createWindow()
{
    mainWindow = new BrowserWindow({//374
        width:       475,
        height:      325,
        maxWidth:    475,
        maxHeight:   325,
        resizable:   false,
        frame:       true,
        icon:        __dirname + "/icon.png"
    });
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    mainWindow.setMenu(null);

    mainWindow.on('closed', function()
    {
        mainWindow = null;
    });

    //mainWindow.on('close', function(event)
    //{
    //    if (!app.isQuiting)
    //    {
    //        event.preventDefault();
    //        mainWindow.hide();
    //    }
    //
    //    return false;
    //});

    mainWindow.on('minimize', function(event)
    {
        event.preventDefault();
        mainWindow.hide();
    });

    // Create tray icon
    tray = new Tray(__dirname + "/tray.png");
    var contextMenu = Menu.buildFromTemplate([

        // Toggle window visibility
        {
            label: pjson.display_name,
            click: function()
            {
                mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
            }
        },

        { type: 'separator' },

        // Quit application
        {
            label: "Quit",
            click: function()
            {
                app.isQuiting = true;
                app.quit();
            }
        }
    ]);
    tray.setTitle(pjson.display_name);
    tray.setToolTip(pjson.display_name);
    tray.setContextMenu(contextMenu);

    tray.on('click', () =>
    {
        mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
    });
}

function init()
{
    console.log("Electron-: Platform: " + process.platform);
    console.log("Electron-: Creating main window...");

    app.on('ready', createWindow);
    app.on('window-all-closed', function()
    {
        if (process.platform != 'darwin')
        {
            app.quit();
        }
    });

    app.on('activate', function()
    {
        if (mainWindow == null)
        {
            createWindow();
        }
    });

    console.log("Electron-: Done!");
}

init();
