// Original: Derek Chilcote-Batto (dac-b@usa.net)
// Web Site: http://www.mixed.net
// Rewritten by: Rich Reel, all8.com
// Re-rewritten by: Lucas, garron.us
// Re-rewritten by: GhettoGirl, ghettogirl.github.io

var count = 0;
var msecsFirst = 0;
var msecsPrevious = 0;
var lengths = [];
var lastTime = null;
var r;

function ResetCount()
{
    count = 0;
    lengths = [];
    lastTime = null;
    document.TAP_DISPLAY.T_AVG.value = "";
    document.TAP_DISPLAY.T_TAP.value = "";
    document.TAP_DISPLAY.T_RESET.blur();
}

function TapForBPM(e)
{
    console.log(e.keyCode);
    if (e.keyCode == 122)
    {
        ResetCount();
        return true;
    }
    
    else
    {
        addBeat();
    }
}

fin = function()
{
    this.flag = r.popup(this.bar.x,
                        this.bar.y,
                        this.bar.value || "0").insertBefore(this);
}

fout = function()
{
    this.flag.animate({opacity: 0}, 300, function()
    {
        this.remove();
    });
}

// avg(list, 0.4, 0.6) takes a mean of the middle 20%.
function avg(listIn, start, end)
{
    var listCopy = listIn.slice(0);
    listCopy.sort(function(a, b)
    {
        return a - b;
    });
    
    var total = 0;
    var l2 = listCopy.slice(Math.floor(start * listCopy.length),
                            Math.floor(end * listCopy.length));
    for (var i = 0; i < l2.length; i++)
    {
        total += l2[i];
    }
    
    return total / l2.length;
}

function addBeat()
{
    document.TAP_DISPLAY.T_WAIT.blur();
    timeSeconds = new Date;
    msecs = timeSeconds.getTime();
    var bpmAvg;
    
    if (lastTime == null)
    {
        lastTime = msecs;
    }
    
    else
    {
        lengths.push(60000 / (msecs - lastTime));
        lastTime = msecs;
    }
    
    if ((msecs - msecsPrevious) > 1000 * document.TAP_DISPLAY.T_WAIT.value)
    {
        ResetCount();
    }
    
    if (count == 0)
    {
        document.TAP_DISPLAY.T_AVG.value = "First Beat";
        document.TAP_DISPLAY.T_TAP.value = "First Beat";
        msecsFirst = msecs;
        count = 1;
    }
    
    else
    {
        bpmAvg = avg(lengths, 0.35, 0.65);
        document.TAP_DISPLAY.T_AVG.value = (Math.round(bpmAvg * 100)) / 100;
        count++;
        document.TAP_DISPLAY.T_TAP.value = count;
    }
    
    msecsPrevious = msecs;
    
    r.clear();
    var m1 = lengths.map(Math.round);
    var m2 = m1.map(function(a)
    {
        return a;
    });
    
    r.barchart(0, 0, 640, 120, [m1]).hover(fin, fout);
    
    return true;
}


function touchStart()
{
    document.getElementById("T_TAP").setAttribute("class", "disp tapped");
    addBeat();
}

function touchEnd()
{
    document.getElementById("T_TAP").setAttribute("class", "disp");
}

document.onkeypress = TapForBPM;

function ini()
{
    document.getElementById("tap_area").addEventListener('touchstart', touchStart, false);
    r = Raphael("chart", 640, 120);
}
